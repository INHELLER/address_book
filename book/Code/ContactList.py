#!/usr/bin/python3
# -*- coding: utf-8 -*-

import Code.LinkedList as LList
import Code.ContactEntry as CEntry


class ContactList:

    def __init__(self):
        self.data = LList.LinkedList()

    def __iter__(self):
        return self.data

    def __getitem__(self, item):
        return self.data[item]

    def insert(self, in_data):
        new_item = CEntry.ContactEntry(in_data)

        return self.insert_entry(new_item)

    def insert_entry(self, in_entry):
        if len(self.data) == 0:
            return self.data.insert(in_entry)

        for item in self.data:
            if in_entry < item.get_data():
                return self.data.insert(in_entry, item, before=True)

        last_node = self.data.get_end()
        return self.data.insert(in_entry, last_node)

    def insert_node(self, in_node):
        entry = in_node.get_data()
        return self.insert_entry(entry)

    def remove(self, in_data):
        return self.data.delete(in_data)

    def remove_node(self, in_node):
        return self.data.delete_by_node(in_node)

    def rename_by_node(self, in_node, in_name):
        curr_node = self.remove_node(in_node)
        curr_node.get_data().rename(in_name)
        self.insert(curr_node)

    def clear(self):
        self.data.clear()

    def find(self, in_pattern, i_type):
        out_list = []
        for item in self.data:
            if item.get_data().match(in_pattern, i_type):
                out_list.append(item)

        if not out_list:
            return None
        else:
            return out_list

    def get_all_nodes(self):
        out_list = []
        for item in self.data:
            out_list.append(item)

        return out_list

    def merge(self, in_contacts):
        for item in in_contacts:
            self.insert_node(item)

    def import_data(self, in_data):
        for item in in_data:
            entry_item = CEntry.ContactEntry(item)
            self.insert_entry(entry_item)

    def export(self):
        out_list = []
        for item in self.data:
            out_list.append(item.get_data().export())

        return out_list

    def draw(self, list_mode=False):
        out_string = []
        for item in self.data:
            out_string.append('\n{}\n'.format(item.get_data().draw()))

        if list_mode:
            return out_string
        else:
            return '\n'.join(out_string)

    def draw_compact(self, list_mode=False):
        out_string = []
        for item in self.data:
            out_string.append('\n{}\n'.format(item.get_data().draw_compact()))

        if list_mode:
            return out_string
        else:
            return '\n'.join(out_string)
