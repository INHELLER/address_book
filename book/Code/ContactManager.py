#!/usr/bin/python3
# -*- coding: utf-8 -*-

import Code.inputUtils as IUtils

# import Code.ContactEntry as CEntry
import Code.ContactList as CList
# import Code.ActionMenu as AMenu


class ContactManager:

    def __init__(self):

        self.contacts = CList.ContactList()
        self.curr_contacts = []
        self.curr_entry = None
        self.curr_focus = []

        self.last_search_info = {'mode': None, 'type': None, 'data': None}

    def create_contact(self):
        new_contact = {}

        new_name = IUtils.get_name_input('Enter new contact name: ', 'Wrong input.')
        if new_name == 'q':
            return True
        new_contact['name'] = new_name

        new_email = IUtils.get_email_input('Enter contact email: ', blank_v=True)
        if new_email == 'q':
            return True

        if new_email:
            new_contact['emails'] = [new_email]
        else:
            new_contact['emails'] = []

        phone_line = 'Enter phone as (xxx) xx-xx-xx: '
        new_phone = IUtils.get_phone_input(phone_line, blank_v=True)
        if new_phone == 'q':
            return True

        if new_phone:
            new_contact['phones'] = [new_phone]
        else:
            new_contact['phones'] = []

        self.curr_entry = self.contacts.insert(new_contact)

        return self.draw_current_entry()

    def current_all_entries(self):
        self.curr_contacts = self.contacts.get_all_nodes()
        return self.draw_compact_currents()

    def find(self, i_type):
        i_mode = 'entry'
        inp_data = ''
        if i_type != 'all':
            line_dict = {'name': 'name', 'emails': 'email',
                         'phones': 'phone as (xxx) xx-xx-xx'}
            help_line = 'Enter part of the {}: '.format(line_dict[i_type])

            inp_data = IUtils.get_search_input(help_line)
            if inp_data == 'q':
                return True
        else:
            i_mode = i_type

        return self.find_or_focus(i_mode, i_type, inp_data)

    def find_or_focus(self, i_mode=None, i_type=None, in_data=None):
        if i_mode is None:
            i_mode = self.last_search_info['mode']
            i_type = self.last_search_info['type']
            in_data = self.last_search_info['data']

        out_data = None
        if i_mode == 'entry':
            self.curr_contacts = self.contacts.find(in_data, i_type)
            if self.curr_contacts is not None:
                out_data = self.draw_compact_currents(True)
        elif i_mode == 'all':
            self.current_all_entries()
            if self.curr_contacts is not None:
                out_data = self.draw_compact_currents(True)
        elif i_mode == 'info':
            out_data = self.focus_info(i_type)

        self.last_search_info['mode'] = i_mode
        self.last_search_info['type'] = i_type
        self.last_search_info['data'] = in_data

        return out_data

    def set_entry_from_currents(self, c_index):
        if c_index < len(self.curr_contacts):
            self.curr_entry = self.curr_contacts[c_index]
            return self.draw_current_entry()
        return False

    def rename_contact(self):
        prompt_line = 'Enter new name (blank for cancel, q to quit): '
        new_name = IUtils.get_name_input(prompt_line, 'Invalid name.', blank_v=True)
        if new_name == 'q':
            return False
        elif new_name == '':
            return True
        self.contacts.rename_by_node(self.curr_entry, new_name)
        return self.draw_current_entry()

    def focus_info(self, i_type):
        if self.curr_entry is None:
            return None
        self.curr_focus = self.curr_entry.get_data().get_info(i_type)
        if not self.curr_focus:
            return None
        else:
            return self.curr_focus

    def get_focus_items(self):
        return self.curr_focus

    def add_info(self, i_type):
        type_dict = {'emails': 'email',
                     'phones': 'phone in format (xxx) xx-xx-xx'}
        prompt_line = 'Enter new {} (blank to cancel, q to quit): '.format(type_dict[i_type])
        if i_type == 'emails':
            new_info = IUtils.get_email_input(prompt_line, 'Invalid input', blank_v=True)
        else:
            new_info = IUtils.get_phone_input(prompt_line, 'Invalid input', blank_v=True)
        if new_info == 'q':
            return False
        elif new_info == '':
            return True

        self.curr_entry.get_data().add_info(new_info, i_type)
        return True

    def clear_info(self, i_type):
        ask_line = 'Do you really want to clear {} (y/n)? '.format(i_type)
        inp_conform = IUtils.get_y_n(ask_line)
        if inp_conform == 'y':
            self.curr_entry.get_data().clear_info(i_type)
            return self.draw_current_entry()

        return False

    def delete_info(self, m_index, i_type):
        if m_index < len(self.curr_focus):
            self.curr_entry.get_data().delete_info(self.curr_focus[m_index], i_type)
            del self.curr_focus[m_index]
            return self.curr_focus

        return False

    def delete_contact(self):
        self.contacts.remove_node(self.curr_entry)
        self.curr_entry = None
        return False

    def delete_from_current(self, c_index):
        if c_index >= len(self.curr_contacts):
            return False

        self.contacts.remove_node(self.curr_contacts[c_index])
        if self.curr_entry is not None:
            if self.curr_entry.get_data() == self.curr_contacts[c_index].get_data():
                self.curr_entry = None
        del self.curr_contacts[c_index]
        return True

    def delete_all_from_currents(self):
        for entry in self.curr_contacts:
            self.contacts.remove_node(entry)
            if self.curr_entry is not None and entry.get_data() == self.curr_entry.get_data():
                self.curr_entry = None
        self.curr_contacts = []

    def db_import(self, in_data):
        self.contacts.import_data(in_data)

    def db_export(self):
        return self.contacts.export()

    def draw_current_entry(self):
        return self.curr_entry.get_data().draw()

    def draw_entry_compact(self):
        return self.curr_entry.get_data().draw_compact()

    def draw_compact_currents(self, list_mode=False):
        out_list = []
        for entry in self.curr_contacts:
            out_list.append(entry.get_data().draw_compact())

        if list_mode:
            return out_list
        else:
            return '\n'.join(out_list)

    def draw_all_contacts(self):
        return self.contacts.draw()
