#!/usr/bin/python3
# -*- coding: utf-8 -*-

import Code.inputUtils as IUtils


class ActionMenu:

    def __init__(self, m_name, q_help='Quit'):
        self.actions = {}
        self.help_lines = {}
        self.act_set = {'q'}
        self.q_help = q_help

        self.name = m_name
        self.pre_line = ''

    def add_function(self, f_key, func, params=None, help_line='Без понятия, что это делает.'):
        if f_key == 'q':
            return False
        self.actions[f_key] = (func, params)
        self.help_lines[f_key] = help_line
        self.act_set.add(f_key)
        return True

    def remove_function(self, f_key):
        if f_key in self.actions and f_key != 'q':
            del self.actions[f_key]
            del self.help_lines[f_key]
            self.act_set.remove(f_key)
            return True
        else:
            return False

    def set_pre_line(self, in_line):
        if isinstance(in_line, str):
            self.pre_line = in_line
        else:
            raise TypeError('Action Menu: Wrong pre line.')

    def clear_pre_line(self):
        self.pre_line = ''

    def step(self):
        self.print_options()
        inp_act = IUtils.get_command_input('> ', 'Bad Command', self.act_set)

        if inp_act == 'q':
            return False

        if self.actions[inp_act][1] is None:
            return self.actions[inp_act][0]()
        else:
            return self.actions[inp_act][0](*self.actions[inp_act][1])

    def run(self):
        is_running = True
        while is_running:
            is_running = self.step()

        return True

    def draw_pre_line(self):
        return self.pre_line

    def draw_actions(self):
        out_line = ['+---']

        line_counter = 1
        for f_key, f_help in self.help_lines.items():
            curr_line = '| {}:\t{}'.format(f_key, f_help)
            out_line.append(curr_line)
            out_line.append('+---{}'.format('---' * line_counter))
            if line_counter < 6:
                line_counter += 1

        out_line.append('| q:\t{}'.format(self.q_help))
        out_line.append('+---{}'.format('---' * line_counter))

        return '\n'.join(out_line)

    def print_options(self):
        print_lines = []
        if self.pre_line:
            print_lines.extend([self.draw_pre_line(), ''])

        print_lines.append(' {}:'.format(self.name))
        print_lines.extend([self.draw_actions(), ''])

        print('\n'.join(print_lines))
