#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re


class ContactEntry:

    def __init__(self, in_data):

        if isinstance(in_data, str):
            self.name = in_data
            self.emails = set()
            self.phones = set()
        else:
            self.name = in_data['name']
            self.emails = set(in_data['emails'])
            self.phones = set(in_data['phones'])

    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return self.name != other.name

    def __lt__(self, other):
        return self.name < other.name

    def __le__(self, other):
        return self.name == other.name

    def __gt__(self, other):
        return self.name > other.name

    def __ge__(self, other):
        return self.name >= other.name

    def is_same(self, other):
        if self.name != other.name:
            return False
        if self.phones != other.phones:
            return False
        if self.emails != other.emails:
            return False
        return True

    def rename(self, new_name):
        self.name = new_name

    def add_info(self, in_data, i_type):
        if i_type == 'emails':
            self.emails.add(in_data)
        elif i_type == 'phones':
            self.phones.add(in_data)

    def get_info(self, i_type='emails'):
        if i_type == 'name':
            return getattr(self, i_type)
        elif hasattr(self, i_type):
            return list(getattr(self, i_type))

        return None

    def delete_info(self, in_str, i_type):
        if i_type == 'emails' and in_str in self.emails:
            self.emails.remove(in_str)
        elif i_type == 'phones' and in_str in self.phones:
            self.phones.remove(in_str)

    def clear_info(self, i_type):
        if i_type == 'phones':
            self.phones = set()
        elif i_type == 'emails':
            self.emails = set()

    def match(self, in_str, i_type):
        if i_type == 'name' and re.search(in_str, getattr(self, i_type),
                                          flags=re.IGNORECASE):
            return True
        elif i_type == 'name':
            return False
        elif not hasattr(self, i_type):
            return False

        curr_focus = getattr(self, i_type)
        for item in curr_focus:
            if re.search(in_str, item, flags=re.IGNORECASE):
                return True

        return False

    def merge(self, other):
        if self != other:
            return False
        self.emails = self.emails & other.emails
        self.phones = self.phones & other.phones

    def import_and_merge(self, in_data):
        other = ContactEntry(in_data)
        self.merge(other)

    def export(self):
        out_data = {'name': self.name, 'emails': list(self.emails),
                    'phones': list(self.phones)}

        return out_data

    def draw(self):
        out_string = ['+----', '| Name: {}'.format(self.name), '+-------']
        first_mails = '| E-Mails: {}'

        if self.emails:
            mails_string = first_mails.format(' '.join(self.emails))
            out_string.append(mails_string)
            out_string.append('+----------')
        first_phones = '| Phones: {}'
        if self.phones:
            phones_string = first_phones.format(' '.join(self.phones))
            out_string.append(phones_string)
            if self.emails:
                out_string.append('+-------------')
            else:
                out_string.append('+----------')

        return '\n'.join(out_string)

    def draw_compact(self):
        out_lines = ['| Name: {}'.format(self.name)]

        if self.emails:
            email = self.emails.pop()
            self.emails.add(email)
            out_lines.append('Email: {}'.format(email))

        if self.phones:
            phone = self.phones.pop()
            self.phones.add(phone)
            out_lines.append('Phone: {}'.format(phone))

        return ' '.join(out_lines)
