#!/usr/bin/python3
# -*- coding: utf-8 -*-


class Node:

    def __init__(self, in_data=None, in_next=None, in_prev=None):
        self.data = in_data
        self.next_node = in_next
        self.prev_node = in_prev

    def get_data(self):
        return self.data

    def get_next(self):
        return self.next_node

    def get_prev(self):
        return self.prev_node

    def set_next(self, next_node):
        self.next_node = next_node

    def set_prev(self, prev_node):
        self.prev_node = prev_node


class LinkedList:

    def __init__(self, head=None):
        self.head = head
        self.end = head
        self.length = 0

    def __len__(self):
        return self.length

    def __getitem__(self, item):
        if item < 0:
            item = len(self) + item

        if self.head is not None:
            out_item = self.head
        else:
            raise IndexError('linked list out of range.')

        for _ in range(1, item+1):
            out_item = out_item.get_next()
            if out_item is None:
                raise IndexError('linked list out of range.')
        return out_item

    def __iter__(self):
        self.curr_iter_item = self.head
        return self

    def __next__(self):
        if self.curr_iter_item is None:
            raise StopIteration
        out_value = self.curr_iter_item
        self.curr_iter_item = self.curr_iter_item.get_next()
        return out_value

    def get_head(self):
        return self.head

    def get_end(self):
        return self.end

    def insert(self, in_data, insert_pos=None, before=False):
        new_node = Node(in_data)
        self.length += 1
        if insert_pos is None:
            new_node.set_next(self.head)
            self.head = new_node
            self.end = new_node
            return new_node

        if before:
            prev_node = insert_pos.get_prev()
            next_node = insert_pos
        else:
            prev_node = insert_pos
            next_node = insert_pos.get_next()

        new_node.set_prev(prev_node)
        new_node.set_next(next_node)
        if prev_node is not None:
            prev_node.set_next(new_node)
        else:
            self.head = new_node

        if next_node is not None:
            next_node.set_prev(new_node)
        else:
            self.end = new_node

        return new_node

    def insert_node(self, in_node, insert_pos=None, before=False):
        return self.insert(in_node.get_data(), insert_pos, before)

    def search(self, in_data):
        current = self.head
        while current:
            if current.get_data == in_data:
                return current
            else:
                current = current.get_next()
        return current

    def delete(self, in_data):
        item = self.search(in_data)
        if item is None:
            return None
        else:
            return self.delete_by_node(item)

    def delete_by_node(self, in_node):
        prev_node = in_node.get_prev()
        next_node = in_node.get_next()

        if prev_node is not None:
            prev_node.set_next(next_node)
        else:
            self.head = next_node

        if next_node is not None:
            next_node.set_prev(prev_node)
        else:
            self.end = prev_node

        if self.length > 0:
            self.length -= 1
        else:
            self.length = 0

        return in_node

    def clear(self):
        self.head = None
        self.end = None
        self.length = 0
