#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import pickle
import json

import Code.inputUtils as IUtils


class FilesManager:

    def __init__(self):
        pass

    def save(self, in_data):
        if not os.path.exists('./Data'):
            os.makedirs('./Data')

        with open('./Data/data.dtb', 'wb') as out_file:
            pickle.dump(in_data, out_file)

        print('Database saved...')

    def load(self):
        if not os.path.exists('./Data/data.dtb'):
            return None

        out_data = None
        with open('./Data/data.dtb', 'rb') as in_file:
            out_data = pickle.load(in_file)
            print('Database loaded...')

        return out_data

    def export(self, in_data, in_path, i_type='json'):
        if i_type == 'txt':
            with open(in_path, 'w', encoding='utf8') as out_file:
                out_file.write(in_data)
            return True
        elif i_type == 'json':
            with open(in_path, 'w', encoding='utf8') as out_file:
                json.dump(in_data, out_file, indent=2)
            return True
