#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import os


def get_command_input(i_line=None, i_err=None, c_set=None):

    if c_set is None:
        c_set = set()

    while True:
        inp_str = input(i_line)
        if inp_str == 'q' or inp_str in c_set:
            return inp_str

        if i_err:
            print(i_err)
        else:
            print('Wrong input.')


def get_number_input(i_line=None, i_err=None, n_type='a',
                     n_min=None, n_max=None, c_set=None):
    # n_type:
    # 'a' - signed int or float
    # 'u' - unsigned int or float
    # 'i' 'ui' - signed/unsigned int
    # 'f' 'uf' - signed/unsigned float
    cases_dict = {'a': ['f', 'uf', 'i', 'ui'],
                  'u': ['f', 'i']}
    re_dict = {'f': r'^-?\d*\.\d+$', 'uf': r'^\d*\.\d+$',
               'i': r'^\d+$', 'ui': r'^\d+$'}
    cast_dict = {'f': float, 'uf': float, 'i': int, 'ui': int}

    if n_type in re_dict:
        test_type = [n_type]
    elif n_type in cases_dict:
        test_type = cases_dict[n_type]
    else:
        raise TypeError('Wrong number input type!')

    if c_set is None:
        c_set = set()

    while True:
        inp_str = input(i_line)

        if inp_str == 'q' or inp_str in c_set:
            return inp_str

        out_num = 0
        for curr_type in test_type:
            if re.match(re_dict[curr_type], inp_str):
                    out_num = cast_dict[curr_type](inp_str)

        if n_min <= out_num <= n_max:
            return out_num

        if i_err:
            print(i_err)
        else:
            print('Wrong number input.')


def get_name_input(i_line=None, i_err=None, blank_v=False):
    while True:
        inp_str = input(i_line)
        if inp_str == 'q' or inp_str or blank_v:
            return inp_str

        if i_err:
            print(i_err)
        else:
            print('Wrong name input.')


def get_email_input(i_line=None, i_err=None, blank_v=False):
    mail_re = re.compile(r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$')

    while True:
        inp_str = input(i_line)

        if inp_str == 'q' or mail_re.match(inp_str):
            return inp_str
        elif inp_str == '' and blank_v:
            return inp_str

        if i_err:
            print(i_err)
        else:
            print('Wrong email input.')


def get_phone_input(i_line=None, i_err=None, blank_v=False):
    phone_re = r'^\(\d{3}\) \d\d-\d\d-\d\d$'
    while True:
        inp_str = input(i_line)
        if inp_str == 'q' or re.match(phone_re, inp_str):
            return inp_str
        elif inp_str == '' and blank_v:
            return inp_str

        if i_err:
            print(i_err)
        else:
            print('Wrong phone input.')


def get_search_input(i_line=None, i_err=None):
    while True:
        inp_str = input(i_line)

        if inp_str == 'q' or inp_str:
            out_str = re.escape(inp_str)
            return out_str

        if i_err:
            print(i_err)
        else:
            print('Empty search string.')

def get_y_n(i_line=None):
    while True:
        inp_str = input(i_line)

        if inp_str == 'y' or inp_str == 'Y':
            return inp_str
        else:
            return 'n'

def get_path_file_input(i_line=None):

    while True:
        inp_path = input(i_line)
        if inp_path == 'q':
            return inp_path
        inp_dir, inp_file = os.path.split(inp_path)
        if os.path.isdir(inp_dir) and inp_file != '':
            return inp_path
        else:
            print('Wrong path.')
