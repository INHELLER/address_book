#!/usr/bin/python3
# -*- coding: utf-8 -*-

import Code.inputUtils as IUtils


class ListMenu:

    def __init__(self, p_items=8, in_data=None, in_func=None,
                 in_params=None, in_commands=None):

        self.p_items = p_items
        self.p_start = 0
        self.p_end = 0

        if in_func is None:
            self.func = None
            self.params = None
        else:
            self.set_function(in_func, in_params)

        if in_data is None:
            self.data = []
            self.data_len = 0
        else:
            self.set_data(in_data)

        self.pre_line = ''

        self.build_ins = {'n': 'Next', 'p': 'Previous', 'q': 'Go Back'}
        self.c_set = set(self.build_ins)
        if in_commands is None:
            self.commands = {}
        else:
            self.add_commands_dict(in_commands)

    def set_items_per_page(self, items_num):
        self.p_items = items_num

    def add_commands_dict(self, in_dict):
        if in_dict is None:
            return

        for c_key, c_val in in_dict.items():
            self.add_command(c_key, c_val)

    def add_command(self, c_key, c_help):
        if c_key in self.build_ins:
            return False

        self.commands[c_key] = c_help
        self.c_set.add(c_key)
        return True

    def remove_command(self, c_key):
        if c_key in self.commands:
            del self.commands[c_key]
            self.c_set.remove(c_key)
            return True

        return False

    def clear_commands(self):
        self.commands = {}
        self.c_set = set(self.build_ins)

    def set_function(self, in_func, in_params=None, in_commands=None):
        self.func = in_func
        self.params = in_params
        self.add_commands_dict(in_commands)

    def clear_function(self):
        self.func = None

    def set_pre_line(self, in_line):
        if isinstance(in_line, str):
            self.pre_line = in_line
        else:
            raise TypeError('List menu: wrong pre line.')

    def clear_pre_line(self):
        self.pre_line = ''

    def set_data(self, in_data):
        if isinstance(in_data, list):
            self.data = in_data
            self.data_len = len(self.data)
            self.first_screen()
        else:
            raise TypeError('List menu: wrong data type.')

    def clear_data(self):
        self.data = []
        self.data_len = 0
        self.first_screen()

    def step(self):
        self.print_output()

        inp_act = IUtils.get_number_input('> ', 'Wrong number or input', 'ui',
                                          1, self.data_len, self.c_set)
        if inp_act == 'q':
            return False
        elif inp_act == 'p':
            self.prev_screen()
            return True
        elif inp_act == 'n':
            self.next_screen()
            return True
        elif isinstance(inp_act, int):
            inp_act -= 1

        if self.params is None:
            quit_signal = self.func(inp_act)
        else:
            quit_signal = self.func(inp_act, *self.params)

        return quit_signal

    def run(self):
        is_running = True
        while is_running:
            is_running = self.step()

        return True

    def first_screen(self):
        self.p_start = 0
        new_end = self.p_items
        if new_end > self.data_len:
            self.p_end = self.data_len
        else:
            self.p_end = new_end

    def prev_screen(self):
        new_start = self.p_start - self.p_items
        if new_start < 0:
            self.p_start = 0
        else:
            self.p_start = new_start

        new_end = new_start + self.p_items
        if new_end > self.data_len:
            self.p_end = self.data_len
        else:
            self.p_end = new_end

    def next_screen(self):
        new_start = self.p_start + self.p_items
        if new_start >= self.data_len > self.p_items:
            self.p_start = self.data_len - self.p_items - 1
        elif self.data_len <= self.p_items:
            self.p_start = 0
        else:
            self.p_start = new_start

        new_end = new_start + self.p_items
        if new_end > self.data_len:
            self.p_end = self.data_len
        else:
            self.p_end = new_end

    def draw_pre_line(self):
        return self.pre_line

    def draw_post_line(self):
        # В начале встроенные.
        build_list = []
        for b_key, b_val in self.build_ins.items():
            build_list.append('{}: {}'.format(b_key, b_val))

        out_list = [', '.join(build_list), ' | ']

        # Потом дополнительные.
        command_list = []
        for c_key, c_val in self.commands.items():
            command_list.append('{}: {}'.format(c_key, c_val))
        out_list.append(', '.join(command_list))

        return ''.join(out_list)

    def draw_items(self):
        out_list = []
        for curr_num in range(self.p_start, self.p_end):
            curr_line = '{}. {}'.format(curr_num+1, self.data[curr_num])
            out_list.append(curr_line)
        return '\n'.join(out_list)

    def print_output(self):
        mid_line = '+------------+'
        print_list = []
        pre = self.draw_pre_line()
        if pre:
            print_list.extend([pre, mid_line])

        print_list.extend([self.draw_items(), mid_line, self.draw_post_line()])
        print('\n'.join(print_list))
