#!/usr/bin/python3
# -*- coding: utf-8 -*-

import Code.ActionMenu as AMenu
import Code.ListMenu as LMenu
import Code.ContactManager as CManager
import Code.FilesManager as FManager
import Code.inputUtils as IUtils


class ContactApplication:

    def __init__(self):
        self.manager = CManager.ContactManager()
        self.entry_text = None
        self.focus_lines = []

        self.file_manager = FManager.FilesManager()

        self.current_menu = None

    def run(self):
        curr_db = self.file_manager.load()
        if curr_db is not None:
            self.manager.db_import(curr_db)

        self.main_menu()

        self.save()

    def main_menu(self):
        current_menu = AMenu.ActionMenu('Main Menu', 'Quit')
        current_menu.set_pre_line('Contact manager.')

        current_menu.add_function('a', self.create_contact, None, 'Add contact')
        current_menu.add_function('l', self.entries_find_del_menu, ['edit', 'all'], 'List contacts...')
        current_menu.add_function('f', self.find_del_menu, ['edit'], 'Find contact(s)...')
        current_menu.add_function('d', self.find_del_menu, ['delete'], 'Delete contact(s)...')
        current_menu.add_function('s', self.save, None, 'Save contacts')
        current_menu.add_function('e', self.export_menu, None, 'Export contacts')


        current_menu.run()

    def edit_menu(self, in_index=None):
        if in_index is not None:
            self.manager.set_entry_from_currents(in_index)

        current_menu = AMenu.ActionMenu('Edit Menu', 'Previous menu')
        current_menu.add_function('r', self.manager.rename_contact,
                                  help_line='Rename contact')
        current_menu.add_function('e', self.add, ['emails'], 'Add email')
        current_menu.add_function('E', self.del_info_menu, ['emails'], 'Remove email')
        current_menu.add_function('p', self.add, ['phones'], 'Add phone')
        current_menu.add_function('P', self.del_info_menu, ['phones'], 'Remove phone...')
        current_menu.add_function('d', self.delete_current, None, 'Delete contact')

        is_running = True
        while is_running:
            self.entry_text = self.manager.draw_current_entry()
            current_menu.set_pre_line(self.entry_text)
            is_running = current_menu.step()

        return True

    def find_del_menu(self, f_mode='edit'):
        menu_dict = {'edit': 'Find', 'delete': 'Delete'}
        curr_menu = AMenu.ActionMenu('{} Menu'.format(menu_dict[f_mode]), 'Previous menu')

        item_line = '{} by name'.format(menu_dict[f_mode])
        curr_menu.add_function('n', self.entries_find_del_menu, [f_mode, 'name'], item_line)

        item_line = '{} by email'.format(menu_dict[f_mode])
        curr_menu.add_function('e', self.entries_find_del_menu, [f_mode, 'emails'], item_line)

        item_line = '{} by phone'.format(menu_dict[f_mode])
        curr_menu.add_function('p', self.entries_find_del_menu, [f_mode, 'phones'], item_line)

        curr_menu.step()

        return True

    def del_info_menu(self, f_type='emails'):
        curr_menu = LMenu.ListMenu()

        curr_menu.set_function(self.delete, ['info', f_type], {'a': 'Delete all'})

        is_running = True
        while is_running:
            found_items = self.manager.focus_info(f_type)
            if found_items is None:
                print('No {} to process.'.format(f_type))
                return True
            self.focus_lines = found_items
            curr_menu.set_pre_line('Found {} {}.'.format(len(self.focus_lines), f_type))
            curr_menu.set_data(self.focus_lines)

            is_running = curr_menu.step()

        return True

    def entries_find_del_menu(self, f_mode='edit', f_type='all'):
        curr_menu = LMenu.ListMenu()

        if f_mode == 'edit':
            curr_menu.set_function(self.edit_menu)
        elif f_mode == 'delete':
            curr_menu.set_function(self.delete, ['entry', None], {'a': 'Delete all'})

        self.manager.find(f_type)
        is_running = True
        while is_running:
            found_items = self.manager.find_or_focus()
            if found_items is None:
                print('No entries found...')
                return True
            self.focus_lines = found_items
            curr_menu.set_pre_line('Found {} entries.'.format(len(self.focus_lines)))

            curr_menu.set_data(self.focus_lines)

            is_running = curr_menu.step()

        return True

    def export_menu(self):
        current_menu = AMenu.ActionMenu('Export Menu')
        current_menu.add_function('t', self.export, ['txt'], 'Export as text...')
        current_menu.add_function('j', self.export, ['json'], 'Export as json..')

        current_menu.step()
        return True

    def delete(self, in_data, d_mode, d_type=None):
        if in_data == 'a' and d_mode == 'entry':
            self.manager.delete_all_from_currents()
            return False
        elif in_data == 'a' and d_mode == 'info':
            self.manager.clear_info(d_type)
            return False
        elif d_mode == 'entry':
            self.manager.delete_from_current(in_data)
        elif d_mode == 'info':
            self.manager.delete_info(in_data, d_type)

        return True

    def delete_current(self):
        self.manager.delete_contact()

    def add(self, data_type='entry'):
        if data_type == 'entry':
            self.manager.create_contact()
        elif data_type == 'phones' or data_type == 'emails':
            self.manager.add_info(data_type)

        return True

    def focus(self, f_mode='all', f_type='all'):
        if f_type == 'all':
            self.focus_lines = self.manager.current_all_entries()
        elif f_mode == 'entry' and f_type in {'names', 'emails', 'phones'}:
            self.manager.find(f_mode)
            self.focus_lines = self.manager.draw_compact_currents()
        elif f_mode == 'info' and f_type in {'emails', 'phones'}:
            self.focus_lines = self.manager.focus_info(f_type)
        elif f_mode == 'old':
            self.focus_lines = self.manager.find_or_focus()

    def list_menu(self, f_mode='entry', f_action='edit'):
        list_menu = LMenu.ListMenu(in_data=self.focus_lines)

        if f_mode == 'entry' and f_action == 'edit':
            list_menu.set_function(self.edit_contact_menu)
        elif f_action == 'delete':
            pass

    def list_all_menu(self):
        self.manager.current_all_entries()
        self.focus_lines = self.manager.draw_compact_currents(True)
        l_menu = LMenu.ListMenu(in_func=self.edit_contact_menu,
                                in_data=self.focus_lines)
        l_menu.run()

    def rename_contact(self):
        quit_status = self.manager.rename_contact()
        self.entry_text = self.manager.draw_current_entry()
        self.current_menu.set_pre_line(self.entry_text)
        return quit_status

    def create_contact(self):
        self.manager.create_contact()
        self.edit_menu()
        return True

    def save(self):
        out_data = self.manager.db_export()
        self.file_manager.save(out_data)

        return True

    def export(self, e_type='json'):
        exp_path = IUtils.get_path_file_input('Enter out file path:')

        if e_type == 'json':
            out_data = self.manager.db_export()
            print(out_data)
        elif e_type == 'txt':
            out_data = self.manager.draw_all_contacts()
            print(out_data)
        else:
            return True

        self.file_manager.export(out_data, exp_path, e_type)

        return True
